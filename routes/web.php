<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

// Complaint
$app->post('/complaint', 'ComplaintController@postComplaint');

// Upload address table
$app->get('/upload_addresses', 'AddressController@uploadForm');
$app->post('/upload_addresses', 'AddressController@uploadSubmit');
$app->get('/updated_at', 'AddressController@getLastUpdate');

// Map
$app->get('/map', 'MapController@getAddresses');
$app->get('/map/{id}', 'MapController@getAddress');

// Statistic
$app->get('/statistic/global', 'StatisticController@getGlobalInfo');

// App
$app->get('/app/install', 'AppController@redirectToInstallUrl');
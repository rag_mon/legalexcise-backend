#!/usr/bin/env bash

apt-get update && apt-get upgrade -y
apt-get install -y apache2 php php-mysql libapache2-mod-php php-mbstring php-zip php-gd php-xml php-curl
apt-get install -y mysql-server mysql-client mysql-common
apt-get install -y git

adduser legalexcise
# ответить на вопросы

cp ./server/legalexcise.conf /etc/apache2/sites-available/legalexcise.conf
mkdir -p /home/legalexcise/www/logs && chown -R legalexcise:legalexcise /home/legalexcise/www

a2dissite 000-default.conf
a2ensite legalexcise.conf
a2enmod rewrite
# в файлах:
# /etc/php/7.0/cli/php.ini
# /etc/php/7.0/apache2/php.ini
# заменить post_max_size=8MB -> 50MB
# заменить upload_max_filesize=2MB -> 10MB
service apache2 restart

mysql_secure_installation
# create database & user (legalexcise)

# install composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer

# install phpunit
wget https://phar.phpunit.de/phpunit-6.5.phar
chmod +x phpunit-6.5.phar
mv phpunit-6.5.phar /usr/local/bin/phpunit
phpunit --version

# install supervisor
apt-get install -y supervisor
cp ./server/legalexcise-worker.conf /etc/supervisor/conf.d/legalexcise-worker.conf
supervisorctl reread
supervisorctl update
supervisorctl start

su legalexcise
cd ~/www
composer update
cp ./.env.example ./.env
# прописать конфиги подключения к БД и прочее в .env
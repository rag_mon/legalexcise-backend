#!/usr/bin/env bash

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# Check is other critical parts of project is running
# TODO: complete this verification script part

# Up docker compose services
echo "Running \"legalexcise-backend\" application docker services..."
if docker-compose up -d --build; then
    echo "Backend API is ready to use."
else
    echo "Error start \"legalexcise-backend\" services."
fi

# Check is need to run DB migrations for first time
# TODO: complete this verification script part
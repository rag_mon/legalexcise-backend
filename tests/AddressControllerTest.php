<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use App\Models\AddressUpdated;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Artisan;

class AddressControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testUploadForm()
    {
        $response = $this->get('/upload_addresses');

        $response
            ->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUploadFormSubmit()
    {
        Artisan::shouldReceive('call')
            ->once()
            ->andSet('command', 'address:parse');

        $uploadedFile = $this->createUploadedFile();

        $this->call('POST', '/upload_addresses', [], [], [
            'table_file' => $uploadedFile,
        ]);

        $this
            ->seeStatusCode(Response::HTTP_OK);
    }

    private function createUploadedFile()
    {
        return new UploadedFile(
            base_path('examples/no_license.xlsx'),
            'no_licenses.xlsx',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            null,
            null,
            true
        );
    }

    public function testGetLastUpdate()
    {
        $addressUpdated = factory(AddressUpdated::class)->create();

        $response = $this->get('/updated_at');

        $response
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJson($addressUpdated->toArray());
    }
}
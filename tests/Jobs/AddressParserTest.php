<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\Address;
use App\Jobs\ParseAddressesJob;
use PhpOffice\PhpSpreadsheet\Reader\Exception as SpreadsheetException;
use Illuminate\Support\Collection;

class AddressParserTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        // Clear addresses table for more effective testing
        Address::truncate();
    }

    /**
     * Test address parser job handle method.
     *
     * @return void
     */
    public function testHandle()
    {
        try {
            // Run address parser job manually
            $parseAddressesJob = new ParseAddressesJob(base_path('examples/short_addresses_example.xlsx'), false);
            $parseAddressesJob->handle();

            // Get all parsed address list from database
            $this->assertParsedAddressesData(Address::all(), $this->getDummyAddressesList());

        } catch (SpreadsheetException $e) {
            $this->assertFalse(false, 'Exception: "PhpOffice\PhpSpreadsheet\Reader\Exception" was thrown. ' . print_r($e));
        }
    }

    /**
     * Assert parsed address data.
     *
     * @param array|Collection $parsedAddresses
     * @param array $dummyAddresses
     * @return void
     */
    protected function assertParsedAddressesData($parsedAddresses, $dummyAddresses)
    {
        $this->assertCount(count($parsedAddresses), $dummyAddresses);

        // Dificult logic for searching on difference in multi level arrays (parsed and dummy)
        $parsedAddresses->each(function ($parsedAddress) use ($dummyAddresses) {
            $parsedAddressArr = array_except($parsedAddress->toArray(), ['id', 'created_at', 'updated_at']);
            $assertionPassed = false;

            // Each all of dummy addresses elements
            foreach ($dummyAddresses as $dummyAddress) {
                // If found any array item without defference then set assertion status passed and break array each
                if (!count(array_diff_assoc($parsedAddressArr, $dummyAddress))) {
                    $assertionPassed = true;
                    break;
                }
            }

            // Check is searched parsed array item is really founded in dummy array
            $this->assertTrue($assertionPassed, 'Parsed address item is not found in dummy array.');
        });
    }

    /**
     * Get test dummy address list array.
     *
     * @return array
     */
    private function getDummyAddressesList()
    {
        return [
            [
                'region_id' => 0,
                'id_code' => '2550305806',
                'address' => 'Ізмаільський р-н, с. Першотравневе, вул. Шкільна, 100, магазин-кафетерій "Наталі"',
                'lng' => null,
                'lat' => null,
                'license' => '151516640311',
                'company' => 'Чумаченко Наталія Дмитрівна',
                'company_type' => 'магазин-кафетерій "Наталі"',
                'license_start_at' => '10.02.2018',
                'license_end_at' => '30.01.2017',
                'license_type' => Address::TYPE_ALCOHOL,
                'status' => Address::STATUS_ACTIVE,
                'public_notices' => null,
                'comment' => null,
                'geocoding_payload' => null,
                'geocoding_at' => null,
            ],
            [
                'region_id' => 0,
                'id_code' => '1838112318',
                'address' => 'Ізмаільський р-н, с. Утконосівка, вул. Шевченка, 41, магазин',
                'lng' => null,
                'lat' => null,
                'license' => '161516640360',
                'company' => 'Чепой Григорій Григорович',
                'company_type' => 'магазин',
                'license_start_at' => '10.02.2018',
                'license_end_at' => '04.02.2018',
                'license_type' => Address::TYPE_BEER,
                'status' => Address::STATUS_ACTIVE,
                'public_notices' => null,
                'comment' => null,
                'geocoding_payload' => null,
                'geocoding_at' => null,
            ],
            [
                'region_id' => 0,
                'id_code' => '1838112318',
                'address' => 'Ізмаільський р-н, с. Утконосівка, вул. Шевченка, 41, магазин',
                'lng' => null,
                'lat' => null,
                'license' => '161516660253',
                'company' => 'Чепой Григорій Григорович',
                'company_type' => 'магазин',
                'license_start_at' => '10.02.2018',
                'license_end_at' => '04.02.2018',
                'license_type' => Address::TYPE_TOBACCO,
                'status' => Address::STATUS_ACTIVE,
                'public_notices' => null,
                'comment' => null,
                'geocoding_payload' => null,
                'geocoding_at' => null,
            ],
        ];
    }

//    /**
//     * Test address parser job handle method with exception.
//     *
//     * @return void
//     */
//    public function testHandleWithException()
//    {
////        try {
////            $parseAddressesJob = new ParseAddressesJob($this->filename, $this->geocodingOnFinish);
////            $parseAddressesJob->handle();
////        } catch (SpreadsheetException $e) {
////            $this->
////        }
//    }
}

<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\Address;
use Illuminate\Http\Response;

class MapControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testGetAddresses()
    {
        factory(Address::class, 1000)->create();

        $response = $this->get('/map');

        $response
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJson();
    }

    public function testGetAddress()
    {
        $address = factory(Address::class)->create();

        $response = $this->get("/map/{$address->id}");

        $response
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJson();
    }
}
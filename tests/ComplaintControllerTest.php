<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\Complaint;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Event;
use App\Events\ComplaintPosted;

class ComplaintControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Using in tear down for remove dummy images from storage.
     *
     * @var array
     */
    private $images;

    public function testPostComplaintNotAnonymously()
    {
        Event::fake();

        $complaint = factory(Complaint::class)
            ->make();

        $response = $this->post('/complaint', $complaint->toArray());

        $response
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJson()
            ->seeInDatabase('complaints', $complaint->toArray());

        Event::assertDispatched(ComplaintPosted::class, function ($e) use ($complaint) {
            return count(array_diff(array_except($e->complaint->toArray(), ['id']), $complaint->toArray())) == 0;
        });
    }

    public function testPostComplaintAnonymously()
    {
        Event::fake();

        $complaint = factory(Complaint::class)
            ->states('anonymous')
            ->make();

        $response = $this->post('/complaint', $complaint->toArray());

        $response
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJson()
            ->seeInDatabase('complaints', $complaint->toArray());

        Event::assertDispatched(ComplaintPosted::class, function ($e) use ($complaint) {
            return count(array_diff(array_except($e->complaint->toArray(), ['id']), $complaint->toArray())) == 0;
        });
    }

    public function testPostComplaintWithImages()
    {
        Event::fake();

        $complaint = factory(Complaint::class)->make();
        $this->images = $this->makeImages();

        $response = $this->call('POST', '/complaint', $complaint->toArray(), [], [
            'image' => $this->images,
        ]);

        $this
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJson()
            ->seeInDatabase('complaints', $complaint->toArray());
        $this->assertImages($this->images, json_decode($response->content()));

        Event::assertDispatched(ComplaintPosted::class, function ($e) use ($complaint) {
            return count(array_diff(array_except($e->complaint->toArray(), ['id']), $complaint->toArray())) == 0;
        });
    }

    public function testPostComplaintWithCoordinates()
    {
        Event::fake();

        $complaint = factory(Complaint::class)
            ->states('with_coordinates')
            ->make();

        $response = $this->post('/complaint', $complaint->toArray());

        $response
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJson()
            ->seeInDatabase('complaints', $complaint->toArray());

        Event::assertDispatched(ComplaintPosted::class, function ($e) use ($complaint) {
            return count(array_diff(array_except($e->complaint->toArray(), ['id']), $complaint->toArray())) == 0;
        });
    }

    protected function assertImages(array $images, $complaint)
    {
        foreach ($images as $image) {
            /** @var UploadedFile $image */
            Storage::disk('local')->assertExists($path = "images/{$image->hashName()}");
            $this->seeInDatabase('images', [
                'path' => $path,
                'complaint_id' => $complaint->id,
            ]);
        }
    }

    protected function makeImages($count = 3)
    {
        $images = [];

        for ($i = 1; $i <= $count; $i++) {
            $images[] = UploadedFile::fake()->image(str_random() . '.jpg');
        }

        return $images;
    }

    public function tearDown()
    {
        parent::tearDown();

        if ($this->images) {
            foreach ($this->images as $image) {
                Storage::disk('local')->delete("images/{$image->hashName()}");
            }
        }
    }
}
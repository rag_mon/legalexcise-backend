# Legalexcise-backend
Version 1.0

> Warning: It's only the backend API for clients in "Legalexcise" project.<br>
> This project can't working correct without other parts (services) of "Legalexcise" (see below).<br>
> If you want to run complete dashboard application - you also need run other parts of project.

> To up all project infrastructure you can run root initial script `sudo ./legalexcise-full-up.sh`.<br>
> **NOTICE:** all initial scripts must be run by super user (`root` or similar privileged system user)<br>
> because scripts perform docker CLI command that needed special privileges,<br>
> otherwise scripts will be executed with errors.

## Project services
- [-] Dashboard application
- [+] **Backend API**
- [-] Client application (phonegap application)

## Micro-services
- queue-worker
- database

## Support features
- addresses map
- address detail information by ID
- receive complaints
- attache photos to complaints
- detect mobile platform and redirect to specific installation URL
- global application statistic
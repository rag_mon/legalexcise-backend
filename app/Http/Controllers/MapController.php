<?php

namespace App\Http\Controllers;

use App\Models\Address;
use Illuminate\Http\Response;

class MapController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Get all addresses list.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAddresses()
    {
        $addresses = Address::all();

        return response()->json($addresses->toArray(), Response::HTTP_OK, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Get the address by ID.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAddress($id)
    {
        /** @var Address $address */
        $address = Address::where('id', $id)->firstOrFail();

        return response()->json($address->toArray());
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Jenssegers\Agent\Agent;

class AppController extends Controller
{
    /**
     * @var Agent
     */
    protected $agent;

    /**
     * @var array
     */
    private $clientUrls;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Agent $agent)
    {
        $this->agent = $agent;
        $this->clientUrls = config('app.client_url');
    }

    /**
     * Redirect to OS specific installation application client url.
     */
    public function redirectToInstallUrl()
    {
        if ($url = $this->getInstallUrlByOS()) {
            return RedirectResponse::create($url);
        } else {
            Log::warning("Device \"{$this->agent->device()}\" is not mobile.");

            abort(Response::HTTP_FORBIDDEN, "Your device {$this->agent->device()} is not support.");
        }
    }

    /**
     * Get install Url by OS name.
     *
     * @return string
     */
    private function getInstallUrlByOS()
    {
        switch ($this->agent->platform()) {
            case 'AndroidOS':
                return $this->clientUrls['android'];
            case 'iOS':
                return $this->clientUrls['ios'];
            default:
                return null;
        }
    }
}

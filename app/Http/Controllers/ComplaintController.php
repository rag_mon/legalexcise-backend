<?php

namespace App\Http\Controllers;

use App\Events\ComplaintPosted;
use App\Models\Complaint;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class ComplaintController extends Controller
{
    public function __construct()
    {
        //
    }

    /**
     * Post complaint request.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postComplaint(Request $request)
    {
//        $this->validate($request, [
//            'type' => 'required|string',
//            'name' => 'required_if:is_anonymously,0',
//            'telephone' => 'required_if:is_anonymously,0',
//            'company' => 'required|string',
//            'email' => 'required_if:is_anonymously,0',
//            'message' => 'required|string|min:3|max:1000',
//            'address_id' => 'exists:addresses,id',
//            'address' => 'nullable|string',
//            'image' => 'array',
//            'image.*' => 'file|mimes:jpg,jpeg,png',
//            'lat' => 'nullable',
//            'lng' => 'nullable',
//            'is_anonymously' => 'required|boolean',
//        ]);

        if ($images = $request->file('image')) {
            $images = $this->storeImages($images);
        }

        /** @var \App\Models\Complaint $complaint */
        $complaint = Complaint::create($request->toArray());
        if ($images) {
            $complaint->images()->saveMany($images);
        }

        event(new ComplaintPosted($complaint));

        return response()->json($complaint->toArray());
    }

    /**
     * @param array $images
     * @return array
     */
    protected function storeImages(array $images)
    {
        $models = [];

        foreach ($images as $image) {
            /** @var UploadedFile $image */
            /** @var Image $model */
            $model = new Image();
            $model->path = $image->store('images');

            $models[] = $model;
        }

        return $models;
    }
}

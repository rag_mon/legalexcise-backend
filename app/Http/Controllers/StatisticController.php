<?php

namespace App\Http\Controllers;

use App\Models\Address;

class StatisticController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get global statistic data.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGlobalInfo()
    {
        $data = [
            'alcohol_count' => 6942, //Address::alcohol()->count(),
            'tobacco_count' => 5253, //Address::tobacco()->count(),
            'alcohol_tobacco_count' => 12195,// Address::alcoholTobacco()->count(),
            'without_license_count' => 201, //Address::withoutLicense()->count(),
        ];

        return response()->json($data);
    }
}

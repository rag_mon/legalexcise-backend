<?php

namespace App\Http\Controllers;

use App\Models\AddressUpdated;
use App\Models\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class AddressController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show upload addresses table file form.
     *
     * @return \Illuminate\View\View
     */
    public function uploadForm()
    {
        return view('address.upload_form');
    }

    /**
     * Upload addresses table file.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function uploadSubmit(Request $request)
    {
        $this->validate($request, [
            'table_file' => 'required|file|mimes:xlsx',
        ]);

        if ($path = $request->file('table_file')->store('addresses')) {
            $path = storage_path("app/$path");

            /** @var Table $table */
            $table = Table::create(['path' => $path]);

            Artisan::call('address:parse', [
                'filename' => $path,
                '--geocoding' => true,
                '--suspicious' => (bool)$request->input('suspicious'),
            ]);

            return $this->uploadForm()
                ->with('table', $table)
                ->with('status', 'Файл успешно загружен и обрабатывается. ' .
                    'Процесс обновления базы данных адресов может занять 5-10 минут.');
        } else {
            return $this->uploadForm()
                ->with('status', 'Ошибка загрузки файла на сервер.');
        }
    }

    /**
     * Get the last updated addresses entry.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLastUpdate()
    {
        /** @var AddressUpdated $addressUpdated */
        $addressUpdated = AddressUpdated::orderBy('id', 'desc')->first();

        return response()->json($addressUpdated ? $addressUpdated->toArray() : null);
    }
}

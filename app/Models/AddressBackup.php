<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * Class AddressBackup
 *
 * @package App\Models
 *
 * @property int id
 * @property string table_name
 * @property Carbon created_at
 *
 * @method $this lastAdded
 */
class AddressBackup extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'table_name',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['create_at'];

    /**
     * Scope addresses backup entities order by created_at.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeLastAdded($query)
    {
        return $query->orderBy('created_at', 'desc');
    }
}

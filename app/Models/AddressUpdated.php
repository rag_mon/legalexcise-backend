<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class Address
 *
 * @package App\Models
 *
 * @property int id
 * @property Carbon $timestamp
 */
class AddressUpdated extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'address_updated';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'timestamp',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['timestamp'];

    public function getTimestampAttribute($value)
    {
        return Carbon::parse($value)->timestamp;
    }
}

<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 *
 * @package App\Models
 *
 * @property int id
 * @property string path
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Image extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'path',
    ];
}

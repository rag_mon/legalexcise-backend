<?php

namespace App\Models;

use App\Entities\UserConfig;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * Class User
 *
 * @package App\Models
 *
 * @property int id
 * @property string name
 * @property string email
 * @property string password
 * @property UserConfig configs
 * @property string rememberToken
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Get configs attribute value.
     *
     * @param string $value
     * @return UserConfig
     */
    public function getConfigsAttribute($value)
    {
        return new UserConfig(json_decode($value, true));
    }

    /**
     * Set configs attribute value.
     *
     * @param UserConfig|array|string|mixed $value
     */
    public function setConfigsAttribute($value)
    {
        if ($value instanceof UserConfig) {
            $this->attributes['configs'] = $value->toJson();
        } elseif (is_array($value)) {
            $this->attributes['configs'] = (new UserConfig($value))->toJson();
        } else {
            $this->attributes['configs'] = $value;
        }
    }

    public function scopeAdmins($query)
    {
        return $query;
    }

    public function scopeNotifiable($query)
    {
        return $query->where('is_notify', true)->orderBy('notify_order');
    }
}

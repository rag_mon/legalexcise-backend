<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class Address
 *
 * @package App\Models
 */
class AddressTemp extends Address
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'addresses_temp';
}

<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Complaint
 *
 * @package App\Models
 *
 * @property int id
 * @property string type
 * @property string name
 * @property string telephone
 * @property string company
 * @property string email
 * @property string message
 * @property string address
 * @property integer address_id
 * @property float lat
 * @property float lng
 * @property Carbon geocoding_at
 * @property Address addressEntry
 * @property Carbon created_at
 * @property string mapLink
 * @property boolean is_anonymously
 */
class Complaint extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'name',
        'telephone',
        'company',
        'email',
        'message',
        'is_anonymously',
        'address',
        'address_id',
        'lng',
        'lat',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany('App\\Models\\Image');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function addressEntry()
    {
        return $this->belongsTo('App\\Models\\Address', 'address_id');
    }

    /**
     * Return is complaint with geo coordinates.
     *
     * @return bool
     */
    public function isWithCoordinates()
    {
        return isset($this->attributes['lat'], $this->attributes['lng']);
    }

    /**
     * @return string
     */
    public function getMapLinkAttribute()
    {
        return "https://maps.google.com/maps?q={$this->lat},{$this->lng}";
    }
}

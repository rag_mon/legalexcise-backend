<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class Address
 *
 * @package App\Models
 *
 * @property int id
 * @property int region_id
 * @property int id_code
 * @property string address
 * @property float lng
 * @property float lat
 * @property string license
 * @property string company
 * @property Carbon license_start_at
 * @property Carbon license_end_at
 * @property string license_type
 * @property string status
 * @property string geocoding_payload
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Carbon geocoding_at
 * @property string comment
 * @property bool isWithCoordinates
 * @property string company_type
 *
 * @method $this withoutCoordinates
 * @method $this alcohol
 * @method $this tobacco
 * @method $this withoutLicense
 * @method $this suspicious
 * @method $this alcoholTobacco
 * @method $this address
 * @method $this withCoordinates
 */
class Address extends Model
{
    const TYPE_ALCOHOL = 'alcohol';
    const TYPE_TOBACCO = 'tobacco';
    const TYPE_MIXED = 'mixed';
    const TYPE_BEER = 'beer';

    /**
     * @var array
     */
    public static $types = [
        self::TYPE_ALCOHOL,
        self::TYPE_TOBACCO,
        self::TYPE_MIXED,
        self::TYPE_BEER,
    ];

    const STATUS_ACTIVE = 'active';
    const STATUS_REARRANGED = 're-arranged';
    const STATUS_CANCELED = 'canceled';
    const STATUS_SUSPICIOUS = 'suspicious';

    /**
     * @var array
     */
    public static $statuses = [
        self::STATUS_ACTIVE,
        self::STATUS_CANCELED,
        self::STATUS_REARRANGED,
        self::STATUS_SUSPICIOUS,
    ];

    const LICENSE_TIME_FORMAT = 'Y-m-d';
    const LICENSE_VIEW_TIME_FORMAT = 'd.m.Y';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'region_id',
        'id_code',
        'address',
        'lng',
        'lat',
        'license',
        'company',
        'company_type',
        'license_start_at',
        'license_end_at',
        'license_type',
        'status',
        'geocoding_payload',
        'geocoding_at',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'public_notices',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'geocoding_payload',
    ];

    /**
     * Scope query to include only addresses without coordinates.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithoutCoordinates($query)
    {
        return $query
            ->whereNull('lat')
            ->whereNull('lng');
    }

    /**
     * Scope query to include only alcohol addresses.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAlcohol($query)
    {
        return $query
            ->where('license_type', self::TYPE_ALCOHOL)
            ->orWhere('license_type', self::TYPE_BEER);
    }

    /**
     * Scope query to include only tobacco addresses.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTobacco($query)
    {
        return $query
            ->where('license_type', self::TYPE_TOBACCO);
    }

    /**
     * Scope query to include only tobacco & alcohol addresses.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAlcoholTobacco($query)
    {
        return $query
            ->where('license_type', self::TYPE_ALCOHOL)
            ->orWhere('license_type', self::TYPE_TOBACCO)
            ->orWhere('license_type', self::TYPE_BEER);
    }

    /**
     * Scope query to include only addresses without license.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithoutLicense($query)
    {
        return $this->scopeSuspicious($query);
    }

    /**
     * Scope query to include only addresses with license.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithLicense($query)
    {
        return $query->where('status', '!=', self::STATUS_SUSPICIOUS);
    }

    /**
     * Scope query to include only suspicious addresses.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSuspicious($query)
    {
        return $query
            ->where('status', self::STATUS_SUSPICIOUS);
    }

    /**
     * Scope query to include only addresses with similar textual address.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $address
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAddress($query, $address)
    {
        return $query
            ->where('address', $address);
    }

    /**
     * Scope query to include only addresses with coordinates.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithCoordinates($query)
    {
        return $query
            ->whereNotNull('lat')
            ->whereNotNull('lng');
    }

    /**
     * Return is addresses with coordinates.
     *
     * @return bool
     */
    public function getIsWithCoordinatesAttribute()
    {
        return isset($this->attributes['lat'], $this->attributes['lng']);
    }

    public function getLicenseStartAtAttribute($value)
    {
        return $this->formatLicenseTimestamp($value);
    }

    public function getLicenseEndAtAttribute($value)
    {
        return $this->formatLicenseTimestamp($value);
    }

    private function formatLicenseTimestamp($value)
    {
        return $value ? Carbon::createFromFormat(self::LICENSE_TIME_FORMAT, $value)->format(self::LICENSE_VIEW_TIME_FORMAT) : null;
    }

    /**
     * Get public notices attribute value.
     *
     * @return null|string
     */
    public function getPublicNoticesAttribute()
    {
        return isset($this->attributes['public_notices'])
            ? json_decode($this->attributes['public_notices'], true)
            : null;
    }
}

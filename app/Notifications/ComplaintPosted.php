<?php

namespace App\Notifications;

use App\Models\Complaint;
use App\Models\Image;
use App\Models\User;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ComplaintPosted extends Notification
{
    /**
     * @var Complaint
     */
    private $complaint;

    /**
     * Create a new notification instance.
     *
     * @param Complaint $complaint
     */
    public function __construct($complaint)
    {
        $this->complaint = $complaint;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  User  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = $this->makeMail($notifiable);

        return $mail;
    }

    /**
     * Make mail.
     *
     * @param User $notifiable
     * @return MailMessage
     */
    protected function makeMail($notifiable)
    {
        $mail = (new MailMessage)
            ->from(env('MAIL_USERNAME'))
            ->subject('Жалоба из приложения "Легальный акциз"')
            ->line($this->complaint->is_anonymously ? 'Анонимная' : '')
            ->line('Тип: ' . $this->complaint->type)
            ->line('ФИО: ' . $this->complaint->name)
            ->line('Компания: ' . $this->complaint->company)
            ->line('Телефон: ' . $this->complaint->telephone)
            ->line('E-mail: ' . $this->complaint->email)
            ->line('Адрес: ' . ($this->complaint->addressEntry
                    ? $this->complaint->addressEntry->address
                    : $this->complaint->address))
            ->line('Текст жалобы: ' . $this->complaint->message);

        // Attaching map and geo coordinates.
        if ($this->complaint->isWithCoordinates()) {
            $mail
                ->line("Координаты: {$this->complaint->lat}°/{$this->complaint->lng}°")
                ->action('Показать на карте', $this->complaint->mapLink);
        }

        // Attaching complaint image files.
        if ($this->complaint->images) {
            foreach ($this->complaint->images as $image) {
                /** @var Image $image */
                $mail->attach($this->getFullImagePath($image->path));
            }
        }

        return $mail;
    }

    /**
     * Get full image path from relative.
     *
     * @param string $path
     * @return string
     */
    private function getFullImagePath($path)
    {
        return storage_path("app/$path");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

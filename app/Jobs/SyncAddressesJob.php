<?php

namespace App\Jobs;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SyncAddressesJob extends Job
{
    /**
     * @var string
     */
    private $syncTableName;

    /**
     * Create a new job instance.
     *
     * @param string $syncTableName
     */
    public function __construct($syncTableName)
    {
        $this->syncTableName = $syncTableName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug('Starting sync addresses...');

        DB::statement(<<<EOF
update addresses, `{$this->syncTableName}` 
set 
  addresses.lat = `{$this->syncTableName}`.lat, 
  addresses.lng = `{$this->syncTableName}`.lng 
where 
  addresses.license = `{$this->syncTableName}`.license
EOF
);

        Log::debug("Finish sync addresses.");
    }
}

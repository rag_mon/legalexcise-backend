<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Artisan;
use Log;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception as SpreadsheetException;
use App\Parser\ChunkReadFilter;
use App\Models\Address;

class ParseSuspiciousAddressesJob extends Job
{
    /**
     * Cell code constants.
     */
    const CELL_COMPANY = 2;
    const CELL_LICENSE = 3;
    const CELL_ADDRESS = 4;
    const CELL_COMMENT = 5;

    /**
     * @var string
     */
    private $inputFileName;

    /**
     * @var string
     */
    private $inputFileType;

    /**
     * @var int
     */
    private $chunkSize;

    /**
     * @var string|array
     */
    private $sheetName;

    /**
     * @var int
     */
    private $startRow;

    /**
     * @var int
     */
    private $endRow;

    /**
     * @var bool
     */
    private $geocodingOnFinish;

    /**
     * Create a new job instance.
     *
     * @param string $filename
     * @param bool $geocodingOnFinish
     */
    public function __construct($filename, $geocodingOnFinish = false)
    {
        $this->inputFileName = $filename;
        $this->inputFileType = 'Xlsx';
        $this->sheetName = 'Лист2';
        $this->chunkSize = 2048;
        $this->startRow = 2;
        $this->endRow = 65536;
        $this->geocodingOnFinish = $geocodingOnFinish;
    }

    /**
     * Execute the job.
     * @return void
     * @throws SpreadsheetException
     */
    public function handle()
    {
        Log::debug("Start parsing suspicious addresses excel file \"$this->inputFileName\".");

        try {
            $filter = $this->getFilter();

            $reader = IOFactory::createReader($this->inputFileType);
            $reader->setReadDataOnly(true);
            $reader->setLoadSheetsOnly($this->sheetName);
            $reader->setReadFilter($filter);

            $worksheetData = $reader->listWorksheetInfo($this->inputFileName);

            $this->endRow = (int)$worksheetData[0]['totalRows'];

            // Loop to read our worksheet in "chunk size" blocks
            for ($startRow = $this->startRow; $startRow <= $this->endRow; $startRow += $this->chunkSize) {
                Log::debug("Loading chunk rows from file (index: $startRow).");

                $filter->setRows($startRow, $this->chunkSize);
                $spreadsheet = $reader->load($this->inputFileName);

                $this->processSpreadsheet($spreadsheet);
            }

            Log::debug('Finish parse suspicious addresses table.');

            if ($this->geocodingOnFinish) {
                Artisan::call('address:geocoding');
            }

        } catch (SpreadsheetException $e) {
            throw $e;
        }
    }

    protected function processSpreadsheet($spreadsheet)
    {
        Log::debug("Starting process the spreadsheet...");

        $worksheet = $spreadsheet->getActiveSheet();
        $highestRow = $worksheet->getHighestRow();

        for ($row = 1; $row <= $highestRow; ++$row) {
            Log::debug("Extract row #$row");

            $address = $this->extractAddressByRow($worksheet, $row);

            if ($address) {
                $this->saveOrUpdateAddress($address);
            } else {
                Log::warning("Incorrect address data in row #$row.");
            }
        }
    }

    private function extractAddressByRow($worksheet, $row)
    {
        // If address not set then return null object
        if ($worksheet->getCellByColumnAndRow(4, $row)->getValue() === NULL) {
            Log::debug("Address row #$row is incorrect. Skipping...");

            return NULL;
        }

        $address = new Address();
        $address->region_id = 0;
        $address->company = $worksheet->getCellByColumnAndRow(self::CELL_COMPANY, $row)->getValue();
        $address->status = Address::STATUS_SUSPICIOUS;
        $address->license_type = Address::TYPE_MIXED;
        $address->license = $worksheet->getCellByColumnAndRow(self::CELL_LICENSE, $row)->getValue();
        $address->address = $worksheet->getCellByColumnAndRow(self::CELL_ADDRESS, $row)->getValue();
        $address->comment = $worksheet->getCellByColumnAndRow(self::CELL_COMMENT, $row)->getValue();

        return $address;
    }

    /**
     * @return ChunkReadFilter
     */
    private function getFilter()
    {
        return new ChunkReadFilter();
    }

    /**
     * Save or update the address.
     *
     * @param Address $address
     * @return Address
     */
    protected function saveOrUpdateAddress($address)
    {
        Log::debug("Update or create address entry by license ID: #{$address->license}");

        return Address::updateOrCreate(['license' => $address->license], $address->toArray());
    }
}

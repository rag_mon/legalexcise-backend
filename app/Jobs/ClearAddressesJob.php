<?php

namespace App\Jobs;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ClearAddressesJob extends Job
{
    /**
     * @var int
     */
    private $regionId;

    /**
     * @var bool
     */
    private $isSuspicious;

    /**
     * Create a new job instance.
     *
     * @param int $regionId
     * @param bool $isSuspicious
     */
    public function __construct($regionId, $isSuspicious = false)
    {
        $this->regionId = $regionId;
        $this->isSuspicious = $isSuspicious;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug('Starting clear addresses table...');

        $sql = "delete from addresses where region_id = '{$this->regionId}' and `status` " .
            ($this->isSuspicious ? '=' : '!=') . " 'suspicious'";

        Log::debug("Run sql query: $sql");

        DB::statement($sql);

        Log::debug('Finished clear addresses table.');
    }
}

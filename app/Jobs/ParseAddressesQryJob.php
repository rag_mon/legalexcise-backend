<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception as SpreadsheetException;
use App\Parser\ChunkReadFilter;
use App\Models\Address;
use PhpOffice\PhpSpreadsheet\Shared\Date as PhpSpreadsheetDate;
use PhpOffice\PhpSpreadsheet\Shared\TimeZone;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Class ParseAddressesQryJob
 *
 * NOTICE: This parser using for sync only addresses timestamp by "license" index field.
 *
 * @package App\Jobs
 */
class ParseAddressesQryJob extends Job
{
    /**
     * Cell code constants.
     */
    const CELL_LICENSE_START_AT = 5;
    const CELL_LICENSE_END_AT = 6;
    const CELL_LICENSE = 7;

    /**
     * @var string
     */
    private $inputFileName;

    /**
     * @var string
     */
    private $inputFileType;

    /**
     * @var int
     */
    private $chunkSize;

    /**
     * @var string|array
     */
    private $sheetName;

    /**
     * @var int
     */
    private $startRow;

    /**
     * @var int
     */
    private $endRow;

    /**
     * Create a new job instance.
     *
     * @param string $filename
     */
    public function __construct($filename)
    {
        $this->inputFileName = $filename;
        $this->inputFileType = 'Xlsx';
        $this->sheetName = 'qryКодыЛицензийДляОтчетов';
        $this->chunkSize = 2048;
        $this->startRow = 2;
        $this->endRow = 65536;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws SpreadsheetException
     */
    public function handle()
    {
        Log::debug("Start parsing addresses timestamps from excel file \"$this->inputFileName\".");

        try {
            $filter = $this->getFilter();

            $reader = IOFactory::createReader($this->inputFileType);
            $reader->setReadDataOnly(true);
            $reader->setLoadSheetsOnly($this->sheetName);
            $reader->setReadFilter($filter);

            $worksheetData = $reader->listWorksheetInfo($this->inputFileName);

            $this->endRow = (int)$worksheetData[0]['totalRows'];

            // Loop to read our worksheet in "chunk size" blocks
            for ($startRow = $this->startRow; $startRow <= $this->endRow; $startRow += $this->chunkSize) {
                Log::debug("Loading chunk rows from file (index: $startRow).");

                $filter->setRows($startRow, $this->chunkSize);
                $spreadsheet = $reader->load($this->inputFileName);

                $this->processSpreadsheet($spreadsheet);
            }

            Log::debug('Finish parse addresses table.');

        } catch (SpreadsheetException $e) {
            throw $e;
        }
    }

    protected function processSpreadsheet($spreadsheet)
    {
        $worksheet = $spreadsheet->getActiveSheet();
        $highestRow = $worksheet->getHighestRow();

        for ($row = 1; $row <= $highestRow; ++$row) {
            $address = $this->extractAddressByRow($worksheet, $row);

            if ($address) {
                $this->updateAddress($address);
            } else {
                Log::warning("Incorrect address data in row #$row.");
            }
        }
    }

    /**
     * Extract address entity from worksheet by row.
     *
     * @param Worksheet $worksheet
     * @param int $row
     * @return Address|null
     */
    private function extractAddressByRow($worksheet, $row)
    {
        // If license not set then return null object
        if ($worksheet->getCellByColumnAndRow(self::CELL_LICENSE, $row)->getValue() === NULL) {
            Log::debug("Address row #$row is incorrect. Skipping...");

            return NULL;
        }

        return [
            'license' => $worksheet->getCellByColumnAndRow(self::CELL_LICENSE, $row)->getValue(),
            'license_start_at' => $this->excelToMysqlDate($worksheet->getCellByColumnAndRow(self::CELL_LICENSE_START_AT, $row)->getValue()),
            'license_end_at' => $this->excelToMysqlDate($worksheet->getCellByColumnAndRow(self::CELL_LICENSE_END_AT, $row)->getValue()),
        ];
    }

    /**
     * Excel timestamp to mysql date.
     *
     * @param int $value
     * @return string
     */
    private function excelToMysqlDate($value)
    {
        return PhpSpreadsheetDate::excelToDateTimeObject($value)->format('Y-m-d');
    }

    /**
     * @return ChunkReadFilter
     */
    private function getFilter()
    {
        return new ChunkReadFilter();
    }

    /**
     * Update the address entry.
     *
     * @param array $address
     * @return Address
     */
    protected function updateAddress($address)
    {
        Log::debug("Update address entry by license ID: #{$address['license']}");

//        dd($address->toArray(), $address->license_start_at, $address->license_end_at);

        return Address::where('license', $address['license'])
            ->update($address);
    }
}

<?php

namespace App\Jobs;

use App\Models\AddressBackup;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BackupAddressesJob extends Job
{
    /**
     * @var int
     */
    private $regionId;

    /**
     * @var bool
     */
    private $isSuspicious;

    /**
     * Create a new job instance.
     *
     * @param int $regionId
     * @param bool $isSuspicious
     */
    public function __construct($regionId, $isSuspicious = false)
    {
        $this->regionId = $regionId;
        $this->isSuspicious = $isSuspicious;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug('Starting backup addresses table...');

        $backupTable = $this->getBackupTableName();

        Log::debug("Backup table with name '$backupTable' will be created.");

        DB::statement("create table `$backupTable` like addresses");
        DB::statement("insert `$backupTable` select * from addresses where region_id = '{$this->regionId}' and `status` "
            . ($this->isSuspicious ? '=' : '!=') . " 'suspicious'");

        $this->logBackup($backupTable);

        Log::debug('Finished creating addresses backup.');
    }

    /**
     * Generate new backup table name.
     *
     * @return string
     */
    private function getBackupTableName()
    {
        return 'addrs_' . date('Y_m_d_h_i_s');
    }

    private function logBackup($backupTable)
    {
        return AddressBackup::create(['table_name' => $backupTable]);
    }
}

<?php

namespace App\Jobs;

use App\Events\AddressesUpdatedEvent;
use App\Models\Address;
use Carbon\Carbon;
use Log;
use Spatie\Geocoder\Facades\Geocoder;

class ParseGeocodingJob extends Job
{
    /**
     * @var int
     */
    private $sleep = 0;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->sleep = config('geocoder.script_delay', 0);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /** @var Address $addressEntry */
        foreach (Address::withoutCoordinates()->cursor() as $addressEntry) {
            // Similar address entry found with set of coordinates.
            if ($similarAddress = Address::address($addressEntry->address)->withCoordinates()->first()) {
                Log::debug("Found similar textual address (ID: #{$similarAddress->id}) for entry ID #{$addressEntry->id}.");

                if ($addressEntry->update([
                    'lat' => $similarAddress->lat,
                    'lng' => $similarAddress->lng,
                ])) {
                    Log::debug("Address entry ID #{$addressEntry->id} coordinates was changed.");
                } else {
                    Log::warning("Address entry ID #{$addressEntry->id} coordinates was not changed.");
                }
            } else {
                $this->detectAttempts($addressEntry);
            }
        }

        Log::debug('Finish fetching addresses geocoding coordinates.');

        event(new AddressesUpdatedEvent());
    }

    /**
     * Detect GEO coordinates attempts.
     *
     * @param Address $addressEntry
     */
    protected function detectAttempts(&$addressEntry)
    {
        $address = $addressEntry->address;

        do {
            /** @var Address $addressEntry */
            $address = $this->filterAddress($address);

            // Incorrect address
            if (!$address)
                break;

            $geocoding_payload = Geocoder::getCoordinatesForAddress($address);

            if ($isSuccess = $geocoding_payload['accuracy'] != 'result_not_found') {
                $addressEntry->update([
                    'geocoding_payload' => json_encode($geocoding_payload, JSON_UNESCAPED_UNICODE),
                    'lng' => $geocoding_payload['lng'],
                    'lat' => $geocoding_payload['lat'],
                    'geocoding_at' => Carbon::now(),
                ]);

                Log::debug("Address entry ID #{$addressEntry->id} coordinates was changed.");
            } else {
                Log::warning("Not found geocoding coordinates for address \"$address\" (Entry ID: #{$addressEntry->id}).");
            }

            // Script delay
            if ($this->sleep)
                sleep($this->sleep);

        } while (!$isSuccess);
    }

    /**
     * Prepare text address for geocoding request.
     *
     * @param string $address
     * @return bool|string
     */
    private function filterAddress($address)
    {
        return ($index = strrpos($address, ',')) >= 0 ? substr($address, 0, $index) : null;
    }
}

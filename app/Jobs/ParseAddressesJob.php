<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception as SpreadsheetException;
use App\Parser\ChunkReadFilter;
use App\Models\Address;
use Exception;
use PhpOffice\PhpSpreadsheet\Shared\Date as PhpSpreadsheetDate;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ParseAddressesJob extends Job
{
    /**
     * Cell code constants.
     */
    const CELL_ID_CODE = 1;
    const CELL_COMPANY = 2;
    const CELL_STATUS = 3;
    const CELL_LICENSE_TYPE = 4;
    const CELL_LICENSE_START_AT = 5;
    const CELL_LICENSE_END_AT = 6;
    const CELL_LICENSE = 7;
    const CELL_ADDRESS = 8;
    const CELL_ADDRESS_TYPE = 9;

    /**
     * @var string
     */
    private $inputFileName;

    /**
     * @var string
     */
    private $inputFileType;

    /**
     * @var int
     */
    private $chunkSize;

    /**
     * @var string|array
     */
    private $sheetName;

    /**
     * @var int
     */
    private $startRow;

    /**
     * @var int
     */
    private $endRow;

    /**
     * @var bool
     */
    private $geocodingOnFinish;

    /**
     * Create a new job instance.
     *
     * @param string $filename
     * @param bool $geocodingOnFinish
     */
    public function __construct($filename, $geocodingOnFinish = false)
    {
        $this->inputFileName = $filename;
        $this->inputFileType = 'Xlsx';
        $this->sheetName = 'qryКодыЛицензийДляОтчетов';
        $this->chunkSize = 2048;
        $this->startRow = 2;
        $this->endRow = 65536;
        $this->geocodingOnFinish = $geocodingOnFinish;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws SpreadsheetException
     */
    public function handle()
    {
        Log::debug("Start parsing addresses excel file \"$this->inputFileName\".");

        try {
            $filter = $this->getFilter();

            $reader = IOFactory::createReader($this->inputFileType);
            $reader->setReadDataOnly(true);
            $reader->setLoadSheetsOnly($this->sheetName);
            $reader->setReadFilter($filter);

            $worksheetData = $reader->listWorksheetInfo($this->inputFileName);

            $this->endRow = (int)$worksheetData[0]['totalRows'];

            // Loop to read our worksheet in "chunk size" blocks
            for ($startRow = $this->startRow; $startRow <= $this->endRow; $startRow += $this->chunkSize) {
                Log::debug("Loading chunk rows from file (index: $startRow).");

                $filter->setRows($startRow, $this->chunkSize);
                $spreadsheet = $reader->load($this->inputFileName);

                $this->processSpreadsheet($spreadsheet);
            }

            Log::debug('Finish parse addresses table.');

            if ($this->geocodingOnFinish) {
                Artisan::call('address:geocoding');
            }

        } catch (SpreadsheetException $e) {
            throw $e;
        }
    }

    protected function processSpreadsheet($spreadsheet)
    {
        $worksheet = $spreadsheet->getActiveSheet();
        $highestRow = $worksheet->getHighestRow();

        for ($row = 1; $row <= $highestRow; ++$row) {
            $address = $this->extractAddressByRow($worksheet, $row);

            if ($address) {
                $this->saveOrUpdateAddress($address);
            } else {
                Log::warning("Incorrect address data in row #$row.");
            }
        }
    }

    /**
     * Extract address entity from worksheet by row.
     *
     * @param Worksheet $worksheet
     * @param int $row
     * @return array|null
     */
    private function extractAddressByRow($worksheet, $row)
    {
        // If license not set then return null object
        if ($worksheet->getCellByColumnAndRow(self::CELL_LICENSE, $row)->getValue() === NULL) {
            Log::debug("Address row #$row is incorrect. Skipping...");

            return NULL;
        }

        return [
            'region_id' => 0,
            'id_code' => $worksheet->getCellByColumnAndRow(self::CELL_ID_CODE, $row)->getValue(),
            'company' => $worksheet->getCellByColumnAndRow(self::CELL_COMPANY, $row)->getValue(),
            'status' => $this->detectStatus($worksheet->getCellByColumnAndRow(self::CELL_STATUS, $row)->getValue()),
            'license_type' => $this->detectType($worksheet->getCellByColumnAndRow(self::CELL_LICENSE_TYPE, $row)->getValue()),
            'license_start_at' => $this->excelToMysqlDate($worksheet->getCellByColumnAndRow(self::CELL_LICENSE_START_AT, $row)->getValue()),
            'license_end_at' => $this->excelToMysqlDate($worksheet->getCellByColumnAndRow(self::CELL_LICENSE_END_AT, $row)->getValue()),
            'license' => $worksheet->getCellByColumnAndRow(self::CELL_LICENSE, $row)->getValue(),
            'address' => $worksheet->getCellByColumnAndRow(self::CELL_ADDRESS, $row)->getValue(),
            'company_type' => $worksheet->getCellByColumnAndRow(self::CELL_ADDRESS_TYPE, $row)->getValue(),
        ];
    }

    /**
     * Excel timestamp to mysql date.
     *
     * @param int $value
     * @return string
     */
    private function excelToMysqlDate($value)
    {
        return PhpSpreadsheetDate::excelToDateTimeObject($value)->format('Y-m-d');
    }

    /**
     * Detect status by title.
     *
     * @param string $value
     * @return string
     * @throws Exception
     */
    private function detectStatus($value)
    {
        switch ($value) {
            case 'Лицензия выдана':
                return Address::STATUS_ACTIVE;
            case 'Аннулирована':
                return Address::STATUS_CANCELED;
            case 'Переоформлена':
                return Address::STATUS_REARRANGED;
            default:
                throw new Exception("Unknown status \"$value\".");
        }
    }

    /**
     * Detect type by title.
     *
     * @param string $value
     * @return string
     * @throws Exception
     */
    private function detectType($value)
    {
        switch ($value) {
            case 'Алкоголь':
                return Address::TYPE_ALCOHOL;
            case 'Пиво':
                return Address::TYPE_BEER;
            case 'Табак':
                return Address::TYPE_TOBACCO;
            default:
                throw new Exception("Unknown type \"$value\".");
        }
    }

    /**
     * @return ChunkReadFilter
     */
    private function getFilter()
    {
        return new ChunkReadFilter();
    }

    /**
     * Save or update the address.
     *
     * @param array $address
     * @return Address
     */
    protected function saveOrUpdateAddress($address)
    {
        Log::debug("Update or create address entry by license ID: #{$address['license']}");

        return Address::updateOrCreate(['license' => $address['license']], $address);
    }
}

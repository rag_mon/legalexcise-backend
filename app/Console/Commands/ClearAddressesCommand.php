<?php

namespace App\Console\Commands;

use App\Jobs\ClearAddressesJob;
use Illuminate\Console\Command;

class ClearAddressesCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'address:clear';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'address:clear
                            {region_id : Addresses region ID}
                            {--suspicious : Suspicious addresses flag}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Clear \"addresses\" table data by filters.";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        dispatch(new ClearAddressesJob(
            $this->argument('region_id'),
            $this->option('suspicious')
        ));
    }
}
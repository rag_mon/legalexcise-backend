<?php

namespace App\Console\Commands;

use App\Jobs\ParseGeocodingJob;
use Illuminate\Console\Command;

class ParseGeocodingCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'address:geocoding';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'address:geocoding';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Parse geo coordinates from exist addresses database.";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        dispatch(new ParseGeocodingJob());
    }
}
<?php

namespace App\Console\Commands;

use App\Jobs\ParseAddressesJob;
use App\Jobs\ParseAddressesQryJob;
use App\Jobs\ParseSuspiciousAddressesJob;
use App\Models\AddressBackup;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class ParseAddressesCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'address:parse';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'address:parse 
                            {filename : Path to the excel file}
                            {--suspicious : Parse the suspicious type address table}
                            {--qry : Parse the "qry" type address table (only timestamps)}
                            {--without-backup : Parse without backup "addresses" table in DB}
                            {--without-clear : Parse without clear "addresses" table in DB}
                            {--without-sync : Parse without sync coordinates from backup}
                            {--geocoding : Run "address:geocoding" command after finish parsing}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Parse the *.xlsx file addresses and import to database.";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        /** @var \App\Jobs\Job $job */

        if (!$this->option('without-backup')) {
            Artisan::call('address:backup', [
                'region_id' => 0,
                '--suspicious' => $this->option('suspicious'),
            ]);
        }

        if (!$this->option('without-clear')) {
            Artisan::call('address:clear', [
                'region_id' => 0,
                '--suspicious' => $this->option('suspicious'),
            ]);
        }

        if ($this->option('qry')) {
            $job = new ParseAddressesQryJob($this->argument('filename'));
        } elseif ($this->option('suspicious')) {
            $job = new ParseSuspiciousAddressesJob(
                $this->argument('filename'),
                $this->option('geocoding')
            );
        } else {
            $job = new ParseAddressesJob(
                $this->argument('filename'),
                $this->option('geocoding')
            );
        }

        dispatch($job);

        // Sync coordinates
        if (!$this->option('without-sync')) {
            /** @var AddressBackup $addressBackup */
            $addressBackup = AddressBackup::lastAdded()->firstOrFail();

            Artisan::call('address:sync', [
                'backup_table' => $addressBackup->table_name,
            ]);
        }
    }
}
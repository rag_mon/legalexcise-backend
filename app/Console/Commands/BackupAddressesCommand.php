<?php

namespace App\Console\Commands;

use App\Jobs\BackupAddressesJob;
use Illuminate\Console\Command;

class BackupAddressesCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'address:backup';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'address:backup
                            {region_id : Addresses region ID}
                            {--suspicious : Suspicious addresses flag}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Backup current \"addresses\" table data.";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        dispatch(new BackupAddressesJob(
            $this->argument('region_id'),
            $this->option('suspicious')
        ));
    }
}
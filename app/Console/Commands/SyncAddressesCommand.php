<?php

namespace App\Console\Commands;

use App\Jobs\SyncAddressesJob;
use Illuminate\Console\Command;

class SyncAddressesCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'address:sync';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'address:sync {backup_table}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Synchronize current addresses coordinates from backup addresses table.";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        dispatch(new SyncAddressesJob($this->argument('backup_table')));
    }
}
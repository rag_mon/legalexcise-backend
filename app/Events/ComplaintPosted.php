<?php

namespace App\Events;

use App\Models\Complaint;

class ComplaintPosted extends Event
{
    /**
     * @var \App\Models\Complaint
     */
    public $complaint;

    /**
     * Create a new event instance.
     *
     * @param \App\Models\Complaint $complaint
     */
    public function __construct(Complaint $complaint)
    {
        $this->complaint = $complaint;
    }
}

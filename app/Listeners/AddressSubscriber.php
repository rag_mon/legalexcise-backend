<?php

namespace App\Listeners;

use App\Models\AddressUpdated;
use Carbon\Carbon;

class AddressSubscriber
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle addresses updated events.
     *
     * @param  \App\Events\AddressesUpdatedEvent  $event
     */
    public function onAddressesUpdated($event)
    {
        AddressUpdated::create(['timestamp' => Carbon::now()]);
    }

    /**
     * Register listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\AddressesUpdatedEvent',
            'App\Listeners\AddressSubscriber@onAddressesUpdated'
        );
    }
}

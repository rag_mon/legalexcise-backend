<?php

namespace App\Listeners;

use App\Models\User;
use App\Notifications\ComplaintPosted;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;
use Spatie\Geocoder\Facades\Geocoder;
use Illuminate\Contracts\Queue\ShouldQueue;

class ComplaintSubscriber implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle contact feedback events.
     *
     * @param  \App\Events\ComplaintPosted  $event
     */
    public function onComplaintPosted($event)
    {
        $complaint = $event->complaint;

        // Get address by coordinates
        if ($complaint->isWithCoordinates()) {
            $complaint->address = $this->getAddressByCoordinates($complaint->lat, $complaint->lng)['formatted_address'];
            $complaint->geocoding_at = Carbon::now();
            $complaint->save();
        }

        $admins = $this->getNotifiableAdmins();

        $this->sendNotify($admins, new ComplaintPosted($complaint));
    }

    private function getAddressByCoordinates($lat, $lng)
    {
        return Geocoder::getAddressForCoordinates($lat, $lng);
    }

    private function sendNotify($admins, $notification)
    {
        Notification::send($admins, $notification);
    }

    /**
     * Get notifiable admins.
     *
     * @return Collection
     */
    private function getNotifiableAdmins()
    {
        return User::admins()->notifiable()->get();
    }

    /**
     * Register listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\ComplaintPosted',
            'App\Listeners\ComplaintSubscriber@onComplaintPosted'
        );
    }
}

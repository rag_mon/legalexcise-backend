<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use App\Models\Address;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('region_id');
            $table->string('id_code');
            $table->string('address');
            $table->float('lng', 9, 6)->nullable();
            $table->float('lat', 9, 6)->nullable();
            $table->string('license');
            $table->string('company');
            $table->date('license_start_at');
            $table->date('license_end_at');
            $table->enum('license_type', Address::$types);
            $table->enum('status', Address::$statuses);
            $table->text('geocoding_payload')->nullable();
            $table->timestamp('geocoding_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->unique(['license']);
            $table->index(['license']);
            $table->index(['region_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}

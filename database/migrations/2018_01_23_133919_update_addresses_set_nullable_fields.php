<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAddressesSetNullableFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->string('id_code')->nullable()->change();
            $table->string('license')->nullable()->change();
            $table->date('license_start_at')->nullable()->change();
            $table->date('license_end_at')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->string('id_code')->change();
            $table->string('license')->change();
            $table->date('license_start_at')->change();
            $table->date('license_end_at')->change();
        });
    }
}

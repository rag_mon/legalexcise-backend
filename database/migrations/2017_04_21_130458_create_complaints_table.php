<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('telephone')->nullable();
            $table->string('company');
            $table->string('email')->nullable();
            $table->text('message');
            $table->string('type');
            $table->boolean('is_anonymously');
            $table->unsignedInteger('address_id')->nullable();
            $table->string('address')->nullable();
            $table->timestamp('created_at')->useCurrent();

            $table->index(['address_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateAddressesAddBeerLicenseType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE addresses CHANGE COLUMN license_type license_type ENUM('alcohol','tobacco','mixed','beer')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE addresses CHANGE COLUMN license_type license_type ENUM('alcohol','tobacco','mixed')");
    }
}

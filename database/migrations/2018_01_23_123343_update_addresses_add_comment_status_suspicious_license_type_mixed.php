<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddressesAddCommentStatusSuspiciousLicenseTypeMixed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE addresses CHANGE COLUMN license_type license_type ENUM('alcohol','tobacco','mixed')");
        DB::statement("ALTER TABLE addresses CHANGE COLUMN status status ENUM('active','canceled','re-arranged','suspicious')");

        Schema::table('addresses', function (Blueprint $table) {
            $table->text('comment')->nullable()->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE addresses CHANGE COLUMN license_type license_type ENUM('alcohol','tobacco')");
        DB::statement("ALTER TABLE addresses CHANGE COLUMN status status ENUM('active','canceled','re-arranged')");

        Schema::table('addresses', function (Blueprint $table) {
            $table->dropColumn('comment');
        });
    }
}

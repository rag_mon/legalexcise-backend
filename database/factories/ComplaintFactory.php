<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Complaint::class, function (Faker\Generator $faker) {
    return [
        'name' => "{$faker->firstName} {$faker->lastName}",
        'telephone' => $faker->phoneNumber,
        'company' => $faker->company,
        'email' => $faker->email,
        'message' => $faker->realText(),
        'type' => $faker->randomElement(['Нет лицензии', 'Антисанитария']),
        'is_anonymously' => false,
    ];
});

$factory->state(App\Models\Complaint::class, 'anonymous', function (Faker\Generator $faker) {
    return [
        'name' => '',
        'telephone' => '',
        'email' => '',
        'is_anonymously' => true,
    ];
});

$factory->state(App\Models\Complaint::class, 'exists_address', function (Faker\Generator $faker) {
    return [
        'address_id' => function () {
            return factory(App\Models\Address::class)->create()->id;
        },
        'address' => null,
    ];
});

$factory->state(App\Models\Complaint::class, 'typed_address', function (Faker\Generator $faker) {
    return [
        'address_id' => null,
        'address' => $faker->address,
    ];
});

$factory->state(App\Models\Complaint::class, 'with_coordinates', function (Faker\Generator $faker) {
    return [
        'lat' => $faker->latitude,
        'lng' => $faker->longitude,
    ];
});
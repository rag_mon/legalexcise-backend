<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Address::class, function (Faker\Generator $faker) {
    return [
        // частичная поддержка регионов "на будущее"
        'region_id' => 0,
        'id_code' => $faker->regexify('/\d{8,10}/'),
        'address' => $faker->address,
        'lng' => $faker->longitude(),
        'lat' => $faker->latitude(),
        'license' => $faker->regexify('/\d{8,10}/'),
        'company' => $faker->company,
        'license_start_at' => $faker->dateTimeBetween('-1 years', 'now'),
        'license_end_at' => $faker->dateTimeBetween('now', '+1 years'),
        'license_type' => $faker->randomElement(\App\Models\Address::$types),
        'status' => $faker->randomElement(\App\Models\Address::$statuses),
        'public_notices' => <<<EOF
{
    "confiscated_goods" : "{$faker->text(50)}",
    "protocol_drawn_up" : "{$faker->text(50)}",
    "financial_sanctions" : "{$faker->text(50)}" 
}
EOF
,
        'geocoding_at' => $faker->dateTimeBetween('-1 year', 'now'),
        'geocoding_payload' => <<<EOF
{
   "results" : [
      {
         "address_components" : [
            {
               "long_name" : "1600",
               "short_name" : "1600",
               "types" : [ "street_number" ]
            },
            {
               "long_name" : "Amphitheatre Pkwy",
               "short_name" : "Amphitheatre Pkwy",
               "types" : [ "route" ]
            },
            {
               "long_name" : "Mountain View",
               "short_name" : "Mountain View",
               "types" : [ "locality", "political" ]
            },
            {
               "long_name" : "Santa Clara County",
               "short_name" : "Santa Clara County",
               "types" : [ "administrative_area_level_2", "political" ]
            },
            {
               "long_name" : "California",
               "short_name" : "CA",
               "types" : [ "administrative_area_level_1", "political" ]
            },
            {
               "long_name" : "United States",
               "short_name" : "US",
               "types" : [ "country", "political" ]
            },
            {
               "long_name" : "94043",
               "short_name" : "94043",
               "types" : [ "postal_code" ]
            }
         ],
         "formatted_address" : "1600 Amphitheatre Parkway, Mountain View, CA 94043, USA",
         "geometry" : {
            "location" : {
               "lat" : 37.4224764,
               "lng" : -122.0842499
            },
            "location_type" : "ROOFTOP",
            "viewport" : {
               "northeast" : {
                  "lat" : 37.4238253802915,
                  "lng" : -122.0829009197085
               },
               "southwest" : {
                  "lat" : 37.4211274197085,
                  "lng" : -122.0855988802915
               }
            }
         },
         "place_id" : "ChIJ2eUgeAK6j4ARbn5u_wAGqWA",
         "types" : [ "street_address" ]
      }
   ],
   "status" : "OK"
}
EOF
    ];
});

$factory->define(App\Models\AddressUpdated::class, function (Faker\Generator $faker) {
    return [
        'timestamp' => $faker->dateTimeBetween('-1 years'),
    ];
});
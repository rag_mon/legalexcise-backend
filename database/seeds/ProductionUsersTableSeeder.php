<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class ProductionUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createSuperAdmin();
    }

    /**
     * Create the super admin entity.
     */
    private function createSuperAdmin()
    {
        User::forceCreate([
            'name' => 'Super Admin',
            'email' => 'ragimov.artyr@gmail.com',
            'password' => app('hash')->make('secret'),
        ]);
    }
}

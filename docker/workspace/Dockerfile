FROM ubuntu:latest

# Need to disable interactive mode and autoset timezone when "tzdata" setting up
ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Kiev

RUN apt-get update && \
    apt-get install -y sudo && \
    apt-get install -y gnupg gnupg2 gnupg1 && \
    apt-get install -y git wget curl mysql-client inetutils-ping

# Install PHP with extensions
RUN apt-get install -y php php-mysql php-xml php-mbstring php-zip php-gd php-curl

# Install composer
RUN curl -s http://getcomposer.org/installer | php && \
    sudo mv composer.phar /usr/bin/composer

# Install PHPUnit
RUN wget https://phar.phpunit.de/phpunit-6.5.3.phar \
    && chmod +x phpunit-6.5.3.phar \
    && sudo mv phpunit-6.5.3.phar /usr/local/bin/phpunit \
    && phpunit --version

# Install NodeJS & npm
RUN curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash - \
    && sudo apt-get install -y nodejs npm

# Install global npm modules
RUN npm install -g bower

# Create user & group
RUN groupadd -g 1000 -r legalexcise && useradd -u 1000 --no-log-init -m -g legalexcise legalexcise
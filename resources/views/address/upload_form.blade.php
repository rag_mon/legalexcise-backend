<html>
<head>
    <title>Legalexcise - Загрузка адресов</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h1>Страница загрузки таблицы адресов</h1>
    <hr>

    <div class="panel panel-default">
        <form action="/upload_addresses" method="post" enctype="multipart/form-data">

            <blockquote class="blockquote">
                Поддерживаемые форматы: xlsx.
            </blockquote>

            <p>
                <input type="file" name="table_file">
            </p>

            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-primary active">
                    <input type="radio" name="suspicious" autocomplete="off" value="0" checked> База адресов (по умолчанию)
                </label>
                <label class="btn btn-warning">
                    <input type="radio" name="suspicious" autocomplete="off"> Подозреваемые адреса
                </label>
            </div>

            @if (isset($status))
                <div class="alert alert-info" role="alert">
                    {{$status}}
                </div>
            @endif

            <div class="text-right">
                <input type="submit" value="Загрузить" class="btn btn-success">
            </div>
        </form>

    </div>
</div>

</body>
</html>